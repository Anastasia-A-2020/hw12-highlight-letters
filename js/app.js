const buttons = document.querySelectorAll(".btn");

document.addEventListener("keydown", onKeyDown);

function onKeyDown(e) {
  for (let button of [...buttons]) {
    if (button.dataset.value.toLocaleLowerCase() === e.key.toLocaleLowerCase()) {
      const selectedButton = document.querySelector(".btn-selected");

      selectedButton?.classList.remove("btn-selected");
      button.classList.add("btn-selected");
    }
  }
}